const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
// const swaggerJSON = required("swagger.json");
// const swaggerUI = required("swagger-ui-express");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

accept request in form or JSON
app.use(bodyParser.urlencoded({ extended: true }));
// app.use("./docs", swaggerUI.serve, swaggerUI.setup(swaggerJSON));
app.use(bodyParser.json());

const db = require("./app/models");
db.client.sync();

require("swagger.json)(app);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
